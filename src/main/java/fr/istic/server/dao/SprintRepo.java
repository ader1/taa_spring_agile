package fr.istic.server.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.istic.server.domain.Sprint;

@Repository
public interface SprintRepo extends JpaRepository<Sprint, Long> {
	
	@Query("SELECT p FROM Sprint p WHERE project_id = :id_project")
    public List<Sprint> findSprintsProject(@Param("id_project") Long id);

}
