package fr.istic.server.dao;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.istic.server.domain.Developer;

@Repository
public interface DeveloperRepo extends JpaRepository<Developer, Long> {
	
	@Query("SELECT d FROM Developer d, Task t WHERE t.id = :id_task and t.developer = d.id")
	public Collection<Developer> findDevelopersTask(@Param("id_task") Long id);
	
	@Query("SELECT p FROM Developer p WHERE nom = :nom_d")
    public Developer findByName(@Param("nom_d") String nom_d);
}
