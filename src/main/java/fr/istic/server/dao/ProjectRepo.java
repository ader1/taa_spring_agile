package fr.istic.server.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.istic.server.domain.Project;

@Repository
public interface ProjectRepo extends JpaRepository<Project, Long> {

}
