package fr.istic.server.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.istic.server.domain.Sprint;
import fr.istic.server.domain.Task;

@Repository
@Transactional
public interface TaskRepo extends JpaRepository<Task, Long> {
	
	@Query("SELECT p FROM Task p WHERE sprint_id = :id_sprint")
    public List<Sprint> find(@Param("id_sprint") Long id);
	
	@Query("SELECT p FROM Task p WHERE sprint_id = :id_sprint")
    public List<Task> findTaskSprint(@Param("id_sprint") Long id);
	
	@Modifying
	@Query("UPDATE Task  SET developer_id = :id_developer WHERE id = :id_task" )
	public void updateTask(@Param ("id_task") Long idTask, @Param ("id_developer") Long idDev);
	
	@Modifying
	@Query("UPDATE Task  SET developer_id = null WHERE id = :id_task" )
	public void deleteDev(@Param ("id_task") Long idTask);
	
}