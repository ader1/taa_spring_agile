package fr.istic.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.mangofactory.swagger.plugin.EnableSwagger;

/**
 * Spring Boot main Application
 *
 */
@SpringBootApplication
@EnableSwagger
@ComponentScan(basePackages = {"fr.istic.server.aspect",
		"fr.istic.server.ws",
		"fr.istic.server.domain",
		"fr.istic.server.config"})
public class Application {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }
}
