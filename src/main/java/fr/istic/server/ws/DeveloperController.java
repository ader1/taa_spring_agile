package fr.istic.server.ws;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wordnik.swagger.annotations.Api;

import fr.istic.server.dao.DeveloperRepo;
import fr.istic.server.dao.TaskRepo;
import fr.istic.server.domain.Developer;

@Api(basePath = "/developer", 
	value = "Dveloper",
	description = "Operations with Developer", 
	produces = "application/json")
@RestController
@RequestMapping(value = "/developers", produces = MediaType.APPLICATION_JSON_VALUE)
public class DeveloperController {
	
	@Autowired
	DeveloperRepo repo;
	
	@Autowired
	TaskRepo repoTask;
	
	
	@RequestMapping(value = "/find/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Developer> getDeveloperById(@PathVariable Long id) {
		Developer developeur= repo.findOne(id);
		return new ResponseEntity<Developer>(developeur, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/findall", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Developer> getAllDeveloper() {
		return repo.findAll();
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void addDeveloper(@RequestBody Developer a) {
		
		repo.save(a);
	}
	
	@RequestMapping(value = "/findByName/{Nom}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Developer>  finByName(@PathVariable("Nom") String nom) {
		Developer developeur = repo.findByName(nom);
		return  new ResponseEntity<Developer>(developeur , HttpStatus.OK);
		
	}
	
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public void deleteDeveloper(@PathVariable Long id) {
		repo.delete(id);
	}
	
	@RequestMapping(value = "/deleteall", method = RequestMethod.DELETE)
	public void deleteAllDeveloper() {
		List<Developer> result = this.getAllDeveloper();
		for (Developer d : result ){
			this.deleteDeveloper(d.getId());
		}
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<Developer> updateDeveloper(Developer d){
		repo.saveAndFlush(d);
		return new ResponseEntity<Developer>(d, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/findDevTask/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Developer> getDeveloperTask(@PathVariable Long id) {
		return (List<Developer>) repo.findDevelopersTask(id);
	}
	
	@RequestMapping(value = "/addDev/{idT}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void addDeveloper(@RequestBody Developer a, @PathVariable Long idT) {
		repo.save(a);
		repoTask.updateTask(idT, a.getId());
	}
	
	@RequestMapping(value = "/deleteDev/{idD}/{idT}", method = RequestMethod.DELETE)
	public void deleteDevTask(@PathVariable Long idD, @PathVariable Long idT) {
		repoTask.deleteDev(idT);
		repo.delete(idD);
	}
	
	
	
}
