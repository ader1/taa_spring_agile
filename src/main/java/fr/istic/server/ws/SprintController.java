package fr.istic.server.ws;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wordnik.swagger.annotations.Api;

import fr.istic.server.dao.*;
import fr.istic.server.domain.*;

@Api(basePath = "/sprint", 
	value = "Sprint",
	description = "Operations with Sprint", 
	produces = "application/json")
@RestController
@RequestMapping(value = "/sprints", produces = MediaType.APPLICATION_JSON_VALUE)
public class SprintController {
	
	@Autowired
	SprintRepo repo;
	
	@Autowired
	ProjectRepo repoProject;
	
	@RequestMapping(value = "/find/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Sprint> getSprintById(@PathVariable Long id) {
		Sprint Sprint= repo.findOne(id);
		return new ResponseEntity<Sprint>(Sprint, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/findall", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Sprint> getAllSprint() {
		return (List<Sprint>) repo.findAll();
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void addSprint(@RequestBody Sprint a) {
		repo.save(a);
	}
	
	@RequestMapping(value = "/add/{idProject}", method = RequestMethod.POST)
	public ResponseEntity<Sprint> addSprint(@RequestBody Sprint a,@PathVariable Long idProject) {
		Project project = repoProject.findOne(idProject);
		a.setProject(project);
		return new ResponseEntity<Sprint>(repo.save(a), HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/find/sprint/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Sprint> getSprintByProject(@PathVariable Long id) {
	         return repo.findSprintsProject(id);
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public void deleteSprint(@PathVariable Long id) {
		repo.delete(id);
	}
	
	@RequestMapping(value = "/deleteall", method = RequestMethod.DELETE)
	public void deleteAllSprint() {
		repo.deleteAllInBatch();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<Sprint> updateSprint(Sprint d){
		repo.saveAndFlush(d);
		return new ResponseEntity<Sprint>(d, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/findSprint/{id}", method = RequestMethod.GET)
	public Collection<Sprint> getSprintsProject(@PathVariable Long id){
		return repo.findSprintsProject(id);
	}
	
}
