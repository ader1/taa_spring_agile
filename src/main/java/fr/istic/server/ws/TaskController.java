package fr.istic.server.ws;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wordnik.swagger.annotations.Api;

import fr.istic.server.dao.*;
import fr.istic.server.domain.*;

@Api(basePath = "/task", 
	value = "task",
	description = "Operations with Task", 
	produces = "application/json")
@RestController
@RequestMapping(value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
public class TaskController {
	
	@Autowired
	TaskRepo repo;
	
	@Autowired
	SprintRepo repoSprint;
	
	@Autowired
	DeveloperRepo repoDev;
	
	@RequestMapping(value = "/find/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Task> getTaskById(@PathVariable Long id) {
		Task Task= repo.findOne(id);
		return new ResponseEntity<Task>(Task, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/findall", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Task> getAllTask() {
		return (List<Task>) repo.findAll();
	}
	
	@RequestMapping(value = "/find/task/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Sprint> getTaskByProject(@PathVariable Long id) {
	         return repo.find(id);
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void addTask(@RequestBody Task a) {
		repo.save(a);
	}
	
	@RequestMapping(value = "/add/{idSprint}", method = RequestMethod.POST)
	public ResponseEntity<Task> addProject(@RequestBody Task a,@PathVariable Long idSprint) {
		Sprint sprint = repoSprint.findOne(idSprint);
		a.setSprint(sprint);
		return new ResponseEntity<Task>(repo.save(a), HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public void deleteTask(@PathVariable Long id) {
		repo.delete(id);
	}
	
	@RequestMapping(value = "/deleteall", method = RequestMethod.DELETE)
	public void deleteAllTask() {
		List<Task> result = this.getAllTask();
		for(Task t : result){
			this.deleteTask(t.getId());
		}
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<Task> updateTask(Task d){
		repo.saveAndFlush(d);
		return new ResponseEntity<Task>(d, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/findTask/{id}", method = RequestMethod.GET)
	public Collection<Task> getTasksSprint(@PathVariable Long id){
		return repo.findTaskSprint(id);
	}
	
	@RequestMapping(value = "/update/{id}/{idv}", method = RequestMethod.PUT)
	public void updateTask(@PathVariable("id") long id ,@PathVariable("idv")  long idv ){
		repo.updateTask(id, idv);
	}
}
