package fr.istic.server.ws;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wordnik.swagger.annotations.Api;

import fr.istic.server.dao.*;
import fr.istic.server.domain.*;

@Api(basePath = "/project", 
	value = "Project",
	description = "Operations with Project", 
	produces = "application/json")
@RestController
@RequestMapping(value = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProjectController {
	
	@Autowired
	ProjectRepo repo;
	
	@RequestMapping(value = "/find/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Project> getProjectById(@PathVariable Long id) {
		Project project= repo.findOne(id);
		return new ResponseEntity<Project>(project, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/findall", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Project> getAllProject() {
		return  (List<Project>) repo.findAll();
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void addProject(@RequestBody Project a) {
		repo.save(a);
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public void deleteProject(@PathVariable Long id) {
		repo.delete(id);
	}
	
	@RequestMapping(value = "/deleteall", method = RequestMethod.DELETE)
	public void deleteAllProject() {
		Collection<Project> result = this.getAllProject();
		for(Project p : result){
			this.deleteProject(p.getId());
		}
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ResponseEntity<Project> updateProject(Project d){
		repo.saveAndFlush(d);
		return new ResponseEntity<Project>(d, HttpStatus.OK);
	}
}
