package fr.istic.server.domain;

import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;



/**
 * Entity implementation class for Entity: Developer
 *
 */
@Entity
@Inheritance
@DiscriminatorColumn(name = "type")
@DiscriminatorValue("Developer")
public class Developer{
    
	private Long id;
	private String nom;
	
	private List<Task> Tasks = new ArrayList<Task>();

	public Developer() {}
	
	public List<Task> Tasks() {
		return Tasks;
	}

	public void setTasks(List<Task> Tasks) {
		this.Tasks = Tasks;
	}

	@Id
	@GeneratedValue
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return this.nom;
	}

	@OneToMany(mappedBy = "developer", cascade = CascadeType.ALL)
	@JsonBackReference("task")
	public List<Task> getTasks() {
		return Tasks;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	@Override
    public String toString() {
        return String.format(
                "Developer [id=%d, nom='%s']",
                id, nom);
    }

}
