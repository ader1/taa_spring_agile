package fr.istic.server.domain;

import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * Entity implementation class for Entity: Sprint
 *
 */
@Entity
public class Sprint  {

	private Long id;
	private String nom;
	private String target;
	
	private List<Task> tasks = new ArrayList<Task>();
	
	private Project project;
	
	public Sprint() {} 
	
	@Id
    @GeneratedValue
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}   
	public String getTarget() {
		return this.target;
	}

	public void setTarget(String target) {
		this.target = target;
	}
	
	@OneToMany(mappedBy="sprint", cascade = CascadeType.ALL)
	@JsonBackReference("task")
	public List<Task> getTasks() {
		return tasks;
	}
	
	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}
	
	@ManyToOne
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
}
