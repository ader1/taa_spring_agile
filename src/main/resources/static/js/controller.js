var mymodal = angular.module('mymodal', []);

mymodal.controller('customersCtrl', [
		'$scope',
		'$http',
		'$window',
		function($scope, $http, $window) {
            $scope.developer= "";
			$scope.toggle = false;

			// diplay BDD in IHM (project)
			$http.get("http://localhost:8080/projects/findall").then(
					function(response) {
						$scope.names = response.data;
						$scope.sprint = undefined;
						$scope.tasks = undefined;
						$scope.developers = undefined ;
					});
		
			// get sprint 
			$scope.getSprint = function(key) {
				$scope.objectS=undefined;
				$scope.objectT= undefined;
				$scope.objectP = key;
				$http
						.get(
								"http://localhost:8080/sprints/find/sprint/"
										+ key.id).then(function(response) {
							$scope.sprints = response.data;
							$scope.tasks =undefined;
							$scope.developers = undefined ;
						});
			};
			
			// get task
			$scope.getTask = function(key) {
				$scope.objectS=key;
				console.log($scope.objectS);
				$http.get("http://localhost:8080/tasks/find/task/" + key.id)
						.then(function(response) {
							$scope.tasks = response.data;
							$scope.developers = undefined ;
						});
			};
			
			// get Developer
			$scope.getDeveloper = function(key) {
				$scope.objectT = key;
				console.log($scope.objectT);
				
				if (key.developer != null){
					//$scope.objectT= undefined;
					var id = key.developer.id;
					console.log(id);
				$http.get(
						"http://localhost:8080/developers/find/"
								+ id).then(function(response) {
					$scope.developers = response.data;
				});
				}
				else { 
					   $scope.developers = undefined ;
				}
			};
			
			
			// Delete Project
			$scope.deleteProject = function(key) {
				$scope.objectP=undefined;
				$scope.objectS=undefined;
				$scope.objectT=undefined;
				
				$http.delete(
						"http://localhost:8080/projects/delete/"+ key.id).then(function(response) {
							
							$http.get("http://localhost:8080/projects/findall").then(
									function(response) {
										$scope.names = response.data;
										$scope.sprint = undefined;
										$scope.tasks = undefined;
										$scope.developers = undefined;
									});
							$http.get(
									"http://localhost:8080/sprints/find/sprint/"
											+ key.id).then(function(response) {
								$scope.sprints = response.data;
								$scope.tasks =undefined;
								$scope.developers = undefined ;
							});
							
				});
			};
			
			// Delete Sprint
			$scope.deleteSprint = function(key) {
				$scope.objectS=undefined;
				$scope.objectT=undefined;
				$http.delete(
						"http://localhost:8080/sprints/delete/"+ key.id).then(function(response) {
							
							$http.get(
									"http://localhost:8080/sprints/find/sprint/"
											+ $scope.objectP.id).then(function(response) {
								$scope.sprints = response.data;
								$scope.tasks = undefined;
								$scope.developers = undefined;
							});
							
							
				});
			};
			
			// Delete Task
			$scope.deleteTask = function(key) {
				$http.delete(
						"http://localhost:8080/tasks/delete/"+ key.id).then(function(response) {
							
							$http.get("http://localhost:8080/tasks/find/task/"+$scope.objectS.id).then(
									function(response) {
										$scope.tasks = response.data;
										$scope.developers = undefined;
										$scope.objectT=undefined;
									});
							
				});
			};
			
			//Delete developer
			$scope.deleteDeveloper = function(key) {
				$http.delete(
						"http://localhost:8080/developers/delete/"+ key.id).then(function(response) {
							
							$http.get("http://localhost:8080/tasks/find/task/"+$scope.objectS.id).then(
									function(response) {
										$scope.tasks = response.data;
										$scope.developers = undefined;
										//$scope.objectT=undefined;
									});
							
				});
			};
			
			
			// project form
			$scope.showModal = false;
			$scope.toggleModal = function() {
				$scope.showModal = !$scope.showModal;
			};

			// add project
			$scope.submit = function(project) {
				console.log(project);
				$scope.objectP=undefined;
				$scope.objectS=undefined;
				var nom = {
					'nom' : project
				};
				$http.post("http://localhost:8080/projects/add", nom).success(
						function(data, status, headers, config) {
							$http.get("http://localhost:8080/projects/findall").then(
									function(response) {
										$scope.names = response.data;
										$scope.sprint = undefined;
										$scope.tasks = undefined;
										$scope.developers = undefined ;
									});
							$scope.toggleModal();
							$window.location.reload();
							

						}).error(function(data, status, header, config) {

				});

				
			};
            
			// sprint form 
			$scope.showModalSprint = false;
			$scope.toggleModalSprint = function() {
				console.log($scope.objectP);
				$scope.showModalSprint = !$scope.showModalSprint;
			};
			
			
			
			// add sprint
			$scope.addSprint = function(nomSprint, target) {
				$scope.objectS=undefined;
				$scope.objectT= undefined;
				var sprint = {
					'nom' : nomSprint,
					'target' : target,
					'project' : {
						'id' : $scope.objectP.id
					}
				};
				$http.post("http://localhost:8080/sprints/add", sprint).success(
						function(data, status, headers, config) {
							$http.get(
									"http://localhost:8080/sprints/find/sprint/"
											+ $scope.objectP.id).then(function(response) {
								$scope.sprints = response.data;
								$scope.tasks = undefined;
								$scope.developers = undefined;
							});
							$scope.toggleModalSprint ();

						}).error(function(data, status, header, config) {

				});	
			};
			
			// task form
			$scope.showModalTask = false;
			$scope.toggleModalTask= function(){
				$scope.showModalTask = !$scope.showModalTask;
			};
			
			// add task
			$scope.addTask = function(Description,Etat,Priorite , NomTask){
				var nom = {'description' : Description ,
						'etat': Etat ,
						'nom': NomTask,
						'priorite' : Priorite, 
						'sprint': {
							'id' : $scope.objectS.id } 
				};
				$http.post("http://localhost:8080/tasks/add", nom ).success(
						function(data, status, headers, config) {
							$http.get(
									"http://localhost:8080/tasks/find/task/"
											+ $scope.objectS.id).then(function(response) {
								$scope.tasks = response.data;
								$scope.developers = undefined;
							});
							$scope.toggleModalTask ();

						}).error(function(data, status, header, config) {

				});	
				
			};
			
			// add developer
			$scope.showModalDeveloper = false;
			$scope.toggleModalDeveloper= function(){
				$scope.showModalDeveloper = !$scope.showModalDeveloper;
			};
			
			// add task
			$scope.addDeveloper= function(NomDeveloper){
				// alert("Hello! I am an alert box!!");
				var id ;
				var d = {'nom' : NomDeveloper ,
						'team':null
				};
				// add developer to bddd(table developer)
				$http.post("http://localhost:8080/developers/add", d).success(
						function(data, status, headers, config) {
							// get the add developer
							$http.get(
									"http://localhost:8080/developers/findByName/"
											+ NomDeveloper).then(function(response) {
								$scope.developer = response.data;
								id = $scope.developer.id;
								console.log($scope.developer);
								console.log($scope.objectT.id);
								console.log(id);
							// attach developer to task
								 $http.put("http://localhost:8080/tasks/update/"+$scope.objectT.id+"/"+
											id)
						            .success(function (data, status, headers, config) {
						            	$http.get("http://localhost:8080/tasks/find/task/" + $scope.objectS.id)
										.then(function(response) {
											$scope.tasks = response.data;
										});
						            })
						            .error(function (data, status, header, config) {
						            });
								 // display developer
								 $http.get(
											"http://localhost:8080/developers/find/"
													+ $scope.developer.id).then(function(response) {
										$scope.developers = response.data;
									});
									
									$scope.toggleModalDeveloper();
							});
							
						}).error(function(data, status, header, config) {

				});	
				
			};
		} ]);

// popup modal
mymodal
		.directive(
				'modal',
				function() {
					return {
						template : '<div class="modal fade">'
								+ '<div class="modal-dialog">'
								+ '<div class="modal-content">'
								+ '<div class="modal-header">'
								+ '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'
								+ '<h4 class="modal-title">{{ title }}</h4>'
								+ '</div>'
								+ '<div class="modal-body" ng-transclude></div>'
								+ '</div>' + '</div>' + '</div>',
						restrict : 'E',
						transclude : true,
						replace : true,
						scope : true,
						link : function postLink(scope, element, attrs) {
							scope.title = attrs.title;

							scope.$watch(attrs.visible, function(value) {
								if (value == true)
									$(element).modal('show');
								else
									$(element).modal('hide');
							});

							$(element).on('shown.bs.modal', function() {
								scope.$apply(function() {
									scope.$parent[attrs.visible] = true;
								});
							});

							$(element).on('hidden.bs.modal', function() {
								scope.$apply(function() {
									scope.$parent[attrs.visible] = false;
								});
							});
						}
					};
				});
