# TP Spring/Angular JS Agile (TAA/GLI)

## Etudiants

Cette application est crée par Ouhammouch Salma & Assal Mohamed Rida 
Etudiants en Master2 MITIC

## Repertoire

Ce repertoire contient l'application TAA Spring(serveur) ainsi que l'application Angular JS (client)

### Organisation du repertoire

Ce repertoire contient l'application Spring boot ainsi que le client Angular JS se situe dans le dossier
src/main/ressources/static


 
# Spring

##Lancement

Pour lancer l'application il faut lancer le serveur de la base de données  ``` ./run-hsqldb-server.sh ```
et après lancer la commande ``` mvn spring-boot:run ``` (Lancé sur le port 8080)
## Accès au service web

Effectuer les requêtes HTTP à l'URL de base en utilisant Swagger (http://localhost:8080/swagger/index.html) ou  http://localhost:8080/
Par exemple


 Type  |              URL             |                                        Action
-------|------------------------------|------------------------------------------------------------------------
  GET  |  /projects/findall           | Retourne la liste de tous les projects
  GET  |  /developers/findall         | Retourne la liste de tous les developers
  GET  |  /sprints/findall            | Retourne la liste de tous les sprints
  GET  |  /tasks/findall              | Retourne la liste de tous les tasks
  
# Angular
 

##Emplacement des fichiers


-Le code source de l'interface AngularJS est situé dans le dossier src/main/resources/static


## Lancement du programme


-Pour lancer angularJS il faut suivre les étapes suivantes :

-1 - ouvrir un terminal à la racine de projet

-2 - aller sur le repertoire src/main/resources/static

-3 - exécuter la commande suivante "sudo npm install http-server -g " pour 

-installer le serveur qui va tourner notre application angularJS 

-4 - exécuter cette commande "http-server -p 8081" pour lancer le serveur 

-5 - sur un navigateur web taper l'url suivant http://localhost:8081/index.html
